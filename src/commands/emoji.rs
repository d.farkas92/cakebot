use serenity::CACHE;

command!(send_emoji(ctx, msg, args) {

    let mut content = &args.single::<String>().unwrap();
    let mut emoji_to_send = "".to_string();

    let guilds = &CACHE.read().guilds;
    for (_, guild) in guilds {
       let gu_unlocked = guild.read();
       for (_, emoji) in &gu_unlocked.emojis {
           if emoji.animated && &emoji.name == content {
                emoji_to_send = format!("<a:{}:{}>", emoji.name, emoji.id);
                break;
           }
       }
    }

    if emoji_to_send.is_empty() {
        emoji_to_send = format!("{} not found\nType \\em list for available animated emojis", content);
    }

    if let Err(why) = msg.channel_id.say(emoji_to_send) {
        println!("Error sending message: {:?}", why);
    }

    if let Err(why) = msg.delete()  {
        println!("Error deleting message: {:?}", why);
    }

});

command!(get_emojis(_ctx, msg, _args) {

    let mut emojis = "".to_string();

    let guilds = &CACHE.read().guilds;
    for (_, guild) in guilds {
       let gu_unlocked = guild.read();
       for (_, emoji) in &gu_unlocked.emojis {
           if emoji.animated {
                emojis = format!("{}\n{}", emojis, emoji.name);
           }
       }
    }

    if let Err(why) = msg.channel_id.say(emojis) {
        println!("Error sending message: {:?}", why);
    }

});