command!(about(_ctx, msg, _args) {
    if let Err(why) = msg.channel_id.say("This is the yummy cake-bot! :yum:") {
        println!("Error sending message: {:?}", why);
    }
});