#[macro_use]
extern crate serenity;

use std::env;
use serenity::{
    framework::standard::{
        help_commands, DispatchError, HelpBehaviour, StandardFramework,
    },
    model::{gateway::{Ready, Game}},
    prelude::*,
};

struct Handler;

impl EventHandler for Handler {
    fn ready(&self, ctx: Context, ready: Ready) {
        let game = Game::playing("\\help for commands");
        ctx.set_game(game);
        println!("{} is connected!", ready.user.name);
    }
}

mod commands;

fn main() {

    let token = env::var("DISCORD_TOKEN").expect(
        "Expected a token in the environment",
    );

    let mut client = Client::new(&token, Handler).expect("Err creating client");

    client.with_framework(
        StandardFramework::new()
        .configure(|c| c
            .on_mention(true)
            .prefix("\\")
            .prefix_only_cmd(commands::small::about))
        .after(|_, msg, command_name, error| {
            match error {
                Ok(()) => println!("Processed command '{} by {}'", command_name, msg.author.name),
                Err(why) => println!("Command '{}' returned error {:?}", command_name, why),
            }
        })
        .unrecognised_command(|_, _, unknown_command_name| {
            println!("Could not find command named '{}'", unknown_command_name);
        })
        .on_dispatch_error(|_ctx, msg, error| {
            if let DispatchError::RateLimited(seconds) = error {
                let _ = msg.channel_id.say(&format!("Try this again in {} seconds.", seconds));
            }
        })
        .command("about", |c| c.cmd(commands::small::about))
        .customised_help(help_commands::with_embeds, |c| {
                c.individual_command_tip("Hello!\n\
                If you want more information about a specific command, just pass the command as argument.")
                .command_not_found_text("Could not find: `{}`.")
                .lacking_permissions(HelpBehaviour::Hide)
                .lacking_role(HelpBehaviour::Nothing)
                .wrong_channel(HelpBehaviour::Nothing)
                 })
        .group("Emoji", |g| g
            .prefixes(vec!["emoji", "em"])
            .default_cmd(commands::emoji::send_emoji)
            .command("send", |c| c
                .known_as("s")
                .desc("A command providing an emoji as response.")
                .cmd(commands::emoji::send_emoji))
            .command("list", |c| c
                .desc("Get available emojis")
                .cmd(commands::emoji::get_emojis)))
    );

    if let Err(why) = client.start() {
        println!("Client error: {:?}", why);
    }
}




